
see

http://piratepad.net/lExBotVYBM

Network
=======

See the big README in the network folder and some graphics in the german
documentation.

Dependencies
============

* ghc
* python
* make

## Fourier analysis
* haskell package: fft (install it with cabal)
* fftw development files (e.g. debian package: libfftw3-dev)

## Plotting
* octave (plus some things for octave to print plots)
* gnuplot

## Doc
* Tex (texlive for example; plus some packages for maths)
* rubber (for compiling tex on the easy way)
* POVRay

